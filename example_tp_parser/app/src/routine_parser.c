/*
 * Copyright (c) 2022 SEBASTIAN BEDIN <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : routine_example_polling.c
 * @date   : May 18, 2022
 * @author : SEBASTIAN BEDIN <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include <arch.h>
#include <config.h>

#include "packet.h"

/********************** macros and definitions *******************************/

#define PERIOD_ticks_                   (100 / APP_CONFIG_PERIOD_ms)
#define DELAY_ticks_                    (500 / APP_CONFIG_PERIOD_ms)

#define BUFFER_SIZE_                    (PACKET_PROTOCOL_MAX_SIZE)

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

static uint8_t tx_buffer_[BUFFER_SIZE_];
static uint16_t tx_size;
static uint8_t rx_buffer_[BUFFER_SIZE_];

static packet_byte_t protocol_buffer[PACKET_PROTOCOL_MAX_SIZE];
static packet_protocol_t protocol;
static packet_message_t *message;

/********************** external data definition *****************************/

extern UART_HandleTypeDef huart3;

/********************** internal functions definition ************************/

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
  if(&huart3 == huart)
  {
//    // RX
//    {
//      HAL_StatusTypeDef rx_status;
//      rx_status = HAL_UARTEx_ReceiveToIdle_IT(&huart3, buffer_, BUFFER_SIZE_);
//      if(HAL_OK != rx_status)
//      {
//        // error
//      }
//    }
  }
}

void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size)
{
  if(&huart3 == huart)
  {
    for(uint16_t i = 0; i < Size; ++i)
    {
      packet_byte_t byte = rx_buffer_[i];
      packet_protocol_feed(&protocol, byte);

      message = packet_protocol_message_read(&protocol);
      if(NULL != message)
      {
        packet_message_process(message);
        packet_protocol_message_write(message);
        memcpy((void*)tx_buffer_, (void*)protocol.buffer, protocol.size);
        tx_size = protocol.size;
        tx_buffer_[tx_size] = '\0';
        tx_size++;
        packet_protocol_set_buffer(&protocol, protocol_buffer, sizeof(protocol_buffer));
      }
    }
    HAL_UARTEx_ReceiveToIdle_IT(&huart3, rx_buffer_, BUFFER_SIZE_);
  }
}

static void packet_protocol_user_cb_(packet_protocol_event_t event)
{
  return;
}

static void init_(void *parameters)
{
  tx_size = 0;

  packet_protocol_init(&protocol, packet_protocol_user_cb_);
  packet_protocol_set_buffer(&protocol, protocol_buffer, sizeof(protocol_buffer));

  // RX
  {
    HAL_StatusTypeDef rx_status;
    rx_status = HAL_UARTEx_ReceiveToIdle_IT(&huart3, rx_buffer_, BUFFER_SIZE_);
    if(HAL_OK != rx_status)
    {
      // error
    }
  }
}

static void loop_(void *parameters)
{
  if(0 < tx_size)
  {
    // TX
    {
      HAL_StatusTypeDef tx_status;
      tx_status = HAL_UART_Transmit_IT(&huart3, tx_buffer_, tx_size);
      if(HAL_OK != tx_status)
      {
        // error
      }
    }
    tx_size = 0;
  }
  return;
}

/********************** external functions definition ************************/

void routine_parser(void *parameters)
{
    static bool _init = true;
    static uint32_t ticks = DELAY_ticks_;
    if(_init)
    {
      _init = false;
      init_(parameters);
    }

    if(0 == ticks)
    {
      ticks = (0 < PERIOD_ticks_) ? PERIOD_ticks_ : 1;
      loop_(parameters);
    }
    ticks--;
}

/********************** end of file ******************************************/
