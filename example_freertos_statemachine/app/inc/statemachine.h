/*
 * Copyright (c) YEAR NOMBRE <MAIL>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @file   : statemachine.h
 * @date   : May 26, 2022
 * @author : NOMBRE <MAIL>
 * @version	v1.0.0
 */

#ifndef INC_STATEMACHINE_H_
#define INC_STATEMACHINE_H_

/********************** CPP guard ********************************************/
#ifdef __cplusplus
extern "C" {
#endif

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

/********************** macros ***********************************************/

/********************** typedef **********************************************/

#define STATEMACHINE_FIRST_STATE                (0)

//#define STATE_MACHINE_GO_TO_STATE(sm, e) \
//        statemachine_go_to_state((sm), (e));\
//        return

typedef struct statemachine_s statemachine_t;

typedef enum
{
  STATEMACHINE_EVENT_ENTRY,
  STATEMACHINE_EVENT_LOOP,
  STATEMACHINE_EVENT_EXIT,
  STATEMACHINE_EVENT_ERROR,
  STATEMACHINE_EVENT__N,
} statemachine_event_t;

typedef void (*statemachine_event_cb_t)(statemachine_t *sm);

typedef struct
{
    statemachine_event_cb_t entry_cb;
    statemachine_event_cb_t loop_cb;
    statemachine_event_cb_t exit_cb;
} statemachine_state_t;

struct statemachine_s
{
    int next_state_id;
    int current_state_id;
    int last_state_id;
    statemachine_state_t *states;
    size_t states_size;
};

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void statemachine_init(statemachine_t *self, statemachine_state_t *states, size_t states_size);

void statemachine_state_init(statemachine_t *self, int state_id, statemachine_event_cb_t entry_cb, statemachine_event_cb_t loop_cb, statemachine_event_cb_t exit_cb);

void statemachine_go_to_state(statemachine_t *self, int state_id);

void statemachine_loop(statemachine_t *self);

/********************** End of CPP guard *************************************/
#ifdef __cplusplus
}
#endif

#endif /* INC_STATEMACHINE_H_ */
/********************** end of file ******************************************/

