/*
 * Copyright (c) 2022 SEBASTIAN BEDIN <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : uart_dma_driver.c
 * @date   : May 23, 2022
 * @author : SEBASTIAN BEDIN <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <app_arch.h>
#include <app_config.h>

#include <uart_dma_driver.h>

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

static bool device_init_(void);

static bool device_driver_init_(uart_dma_driver_t *driver);

static bool channel_send_(uart_dma_driver_t *self, uart_dma_driver_tx_channel_t *channel);

static uart_dma_driver_tx_channel_t *current_channel_(uart_dma_driver_t *self);

static uint16_t current_channel_remaining_size_(uart_dma_driver_t *self);

static bool current_channel_is_full_(uart_dma_driver_t *self);

static bool current_channel_is_empty_(uart_dma_driver_t *self);

static bool current_channel_is_free_(uart_dma_driver_t *self);

static bool current_channel_is_busy_(uart_dma_driver_t *self);

static uint16_t current_channel_copy_buffer_(uart_dma_driver_t *self, uint8_t const *const buffer, uint16_t size);

static void current_channel_send_(uart_dma_driver_t *self);

static void channel_swap_(uart_dma_driver_t *self);

static void uart_dma_driver_rx_user_cb_empty_(uint8_t const *const buffer, uint16_t size);

/********************** internal data definition *****************************/

struct
{
    uart_dma_driver_t *drivers[UART_DMA_DRIVER_CONFIG_DEVICES];
    size_t drivers_size;
} device_;

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static bool device_init_(void)
{
  static bool already_initialized = false;
  if(true == already_initialized)
  {
    return;
  }
  already_initialized = true;

  for(int i = 0; i < UART_DMA_DRIVER_CONFIG_DEVICES; ++i)
  {
    device_.drivers[i] = NULL;
  }
  device_.drivers_size = 0;
}

static bool device_driver_init_(uart_dma_driver_t *driver)
{
  while(UART_DMA_DRIVER_CONFIG_DEVICES <= device_.drivers_size);

  device_.drivers[device_.drivers_size] = driver;
  device_.drivers_size++;
}

/*
 * ISR
 */





/*
 * TX
 */

static bool channel_send_(uart_dma_driver_t *self, uart_dma_driver_tx_channel_t *channel)
{
  if(NULL != self->tx.transfer_channel)
  {
     return;
  }

  self->tx.transfer_channel = channel;
  self->tx.transfer_channel->flag_free = false;
//  self->tx.transfer_channel->flag_timeout = ;

  HAL_StatusTypeDef tx_status;
  tx_status = HAL_UART_Transmit_DMA(self->uart,
                                    self->tx.transfer_channel->buffer,
                                    self->tx.transfer_channel->size);
  if (HAL_OK == tx_status)
  {
    self->tx.flag_timeout = false;
    self->tx.ticks = 0;
  }
  else
  {
    // error
  }
}

static uart_dma_driver_tx_channel_t *current_channel_(uart_dma_driver_t *self)
{
  return self->tx.primary_channel;
}

static uint16_t current_channel_remaining_size_(uart_dma_driver_t *self)
{
  uart_dma_driver_tx_channel_t *channel = current_channel_(self);
  return self->tx.buffer_size - channel->size;
}

static bool current_channel_is_full_(uart_dma_driver_t *self)
{
  uart_dma_driver_tx_channel_t *channel = current_channel_(self);
  uint16_t remaining_size = current_channel_remaining_size_(self);
  return (0 == remaining_size);
}

static bool current_channel_is_empty_(uart_dma_driver_t *self)
{
  uart_dma_driver_tx_channel_t *channel = current_channel_(self);
  return (0 == channel->size);
}

static bool current_channel_is_free_(uart_dma_driver_t *self)
{
  uart_dma_driver_tx_channel_t *channel = current_channel_(self);
  return channel->flag_free;
}

static bool current_channel_is_busy_(uart_dma_driver_t *self)
{
  return !current_channel_is_free_(self);
}

static uint16_t current_channel_copy_buffer_(uart_dma_driver_t *self, uint8_t const *const buffer, uint16_t size)
{
  uart_dma_driver_tx_channel_t *channel = current_channel_(self);
  uint16_t remaining_size = current_channel_remaining_size_(self);
  uint16_t size_to_copy = (size <= remaining_size) ? size : remaining_size;

  memcpy((channel->buffer + channel->size), buffer, size_to_copy);
  channel->size += size_to_copy;
  return size_to_copy;
}

static void current_channel_send_(uart_dma_driver_t *self)
{
  channel_send_(self, self->tx.primary_channel);
}

static void channel_swap_(uart_dma_driver_t *self)
{
  uart_dma_driver_tx_channel_t *channel;
  channel = self->tx.primary_channel;
  self->tx.primary_channel = self->tx.secondary_channel;
  self->tx.secondary_channel = channel;
}

static void driver_tx_isr_(uart_dma_driver_t *self, UART_HandleTypeDef *huart)
{
  if (huart->Instance != self->uart->Instance)
  {
    return;
  }

  self->tx.transfer_channel->flag_free = true;
  self->tx.transfer_channel->size = 0;
  self->tx.transfer_channel = NULL;

  if(0 < self->tx.secondary_channel->size)
  {
    channel_send_(self, self->tx.secondary_channel);
  }
}

/*
 * RX
 */

static void uart_dma_driver_rx_user_cb_empty_(uint8_t const *const buffer, uint16_t size)
{
  return;
}

static void driver_rx_isr_(uart_dma_driver_t *self, UART_HandleTypeDef *huart, uint16_t size)
{
  if (huart->Instance != self->uart->Instance)
  {
    return;
  }

  uint32_t len;

  if (size != self->rx.last_size)
  {
    if (size > self->rx.last_size)
    {
      len = size - self->rx.last_size;
      self->rx.user_cb(self->rx.buffer + self->rx.last_size, len);
    }
    else
    {
      len = self->rx.buffer_size - self->rx.last_size;
      self->rx.user_cb(self->rx.buffer + self->rx.last_size, len);

      if (0 < size)
      {
        len = size;
        self->rx.user_cb(self->rx.buffer, len);
      }
    }
  }
  self->rx.last_size = size;
}

/*
 * SysTick
 */

static void driver_systick_isr_(uart_dma_driver_t *self)
{
  if(false == current_channel_is_empty_(self))
  {
    self->tx.ticks++;
    if(self->tx.timeout_ticks <= self->tx.ticks)
    {
      self->tx.ticks = self->tx.timeout_ticks;
      self->tx.flag_timeout = true;

      if(false == self->tx.systick_mutex)
      {
        if(NULL == self->tx.transfer_channel)
        {
          current_channel_send_(self);
          channel_swap_(self);
        }
      }
    }
  }
}

/********************** external functions definition ************************/

void uart_dma_driver_tx_isr(UART_HandleTypeDef *huart)
{
  for(int i = 0; i < device_.drivers_size; ++i)
  {
    driver_tx_isr_(device_.drivers[i], huart);
  }
}

void uart_dma_driver_rx_isr(UART_HandleTypeDef *huart, uint16_t size)
{
  for(int i = 0; i < device_.drivers_size; ++i)
  {
    driver_rx_isr_(device_.drivers[i], huart, size);
  }
}

void uart_dma_driver_systick_isr(void)
{
  for(int i = 0; i < device_.drivers_size; ++i)
  {
    driver_systick_isr_(device_.drivers[i]);
  }
}

uint16_t uart_dma_driver_send(uart_dma_driver_t *self, uint8_t const *buffer, uint16_t size)
{
  uint16_t size_copied = 0;

  // Primary channel
  {
    self->tx.systick_mutex = true;
    {
      if (current_channel_is_busy_(self))
      {
        // i cant copy nothing
        self->tx.systick_mutex = false;
        return size_copied;
      }
      // else is free
      size_copied += current_channel_copy_buffer_(self, buffer, size);
      if (false == current_channel_is_full_(self))
      {
        if(true == self->tx.flag_timeout)
        {
          current_channel_send_(self);
          channel_swap_(self);
        }
        self->tx.systick_mutex = false;
        return size_copied;
      }
      // else is full
      current_channel_send_(self);
      channel_swap_(self);
    }
    self->tx.systick_mutex = false;
  }

  // Update buffer and size
  {
    buffer = buffer + size_copied;
    size -= size_copied;
  }

  // Secondary channel
  {
    if (current_channel_is_busy_(self))
    {
      // i cant cpy nothing
      return size_copied;
    }
    // else is free
    size_copied += current_channel_copy_buffer_(self, buffer, size);
    if (false == current_channel_is_full_(self))
    {
      return size_copied;
    }
    // else is full
    current_channel_send_(self);
    channel_swap_(self);
  }

  return size_copied;
}

void uart_dma_driver_receive_start(uart_dma_driver_t *self, uart_dma_driver_rx_user_cb_t user_cb)
{
  if (NULL == user_cb)
  {
    user_cb = uart_dma_driver_rx_user_cb_empty_;
  }
  self->rx.user_cb = user_cb;

  HAL_StatusTypeDef rx_status;
  rx_status = HAL_UARTEx_ReceiveToIdle_DMA(self->uart, self->rx.buffer, self->rx.buffer_size);
  if (HAL_OK != rx_status)
  {
    // error
  }
}

void uart_dma_driver_init(uart_dma_driver_t *self, uart_dma_driver_config_t *config)
{
  device_init_();

  self->uart = config->uart;

  // RX
  {
    self->rx.buffer = config->rx.buffer;
    self->rx.buffer_size = config->rx.buffer_size;

    self->rx.user_cb = uart_dma_driver_rx_user_cb_empty_;
    self->rx.last_size = 0;
  }

  // TX
  {
    self->tx.buffer_size = config->tx.buffer_size;

    self->tx.channels[0].buffer = config->tx.buffer_a;
    self->tx.channels[0].flag_free = true;
    self->tx.channels[0].size = 0;

    self->tx.channels[1].buffer = config->tx.buffer_b;
    self->tx.channels[1].flag_free = true;
    self->tx.channels[1].size = 0;

    self->tx.primary_channel = &(self->tx.channels[0]);
    self->tx.secondary_channel = &(self->tx.channels[1]);
    self->tx.transfer_channel = NULL;

    self->tx.timeout_ticks = config->tx.timeout_ticks;
    self->tx.systick_mutex = false;
  }

  device_driver_init_(self);
}

/********************** end of file ******************************************/
