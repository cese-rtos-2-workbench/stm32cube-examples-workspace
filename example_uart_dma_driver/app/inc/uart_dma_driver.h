/*
 * Copyright (c) 2022 SEBASTIAN BEDIN <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @file   : uart_dma_driver.h
 * @date   : May 23, 2022
 * @author : SEBASTIAN BEDIN <sebabedin@gmail.com>
 * @version	v1.0.0
 */

#ifndef INC_UART_DMA_DRIVER_H_
#define INC_UART_DMA_DRIVER_H_

/********************** CPP guard ********************************************/
#ifdef __cplusplus
extern "C" {
#endif

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

/********************** macros ***********************************************/

#define UART_DMA_DRIVER_CONFIG_DEVICES          (2)

/********************** typedef **********************************************/

typedef struct
{
    UART_HandleTypeDef *uart;
    struct
    {
      uint8_t *buffer;
      uint32_t buffer_size;
    } rx;
    struct
    {
      uint8_t *buffer_a;
      uint8_t *buffer_b;
      uint32_t buffer_size;
      uint32_t timeout_ticks;
    } tx;
} uart_dma_driver_config_t;

typedef void (*uart_dma_driver_rx_user_cb_t)(uint8_t const *const buffer, uint16_t size);

typedef struct
{
    uint8_t *buffer;
    uint16_t size;
    bool flag_free;
} uart_dma_driver_tx_channel_t;

typedef struct
{
    UART_HandleTypeDef *uart;

    struct
    {
      uint8_t *buffer;
      uint16_t buffer_size;
      uart_dma_driver_rx_user_cb_t user_cb;
      uint16_t last_size;
    } rx;

    struct
    {
      uart_dma_driver_tx_channel_t channels[2];
      uart_dma_driver_tx_channel_t *primary_channel;
      uart_dma_driver_tx_channel_t *secondary_channel;
      uart_dma_driver_tx_channel_t *transfer_channel;
      uint16_t buffer_size;
      uint32_t timeout_ticks;
      uint32_t ticks;
      bool flag_timeout;
      bool systick_mutex;
    } tx;

} uart_dma_driver_t;

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void uart_dma_driver_tx_isr(UART_HandleTypeDef *huart);

void uart_dma_driver_rx_isr(UART_HandleTypeDef *huart, uint16_t size);

void uart_dma_driver_systick_isr(void);

uint16_t uart_dma_driver_send(uart_dma_driver_t *self, uint8_t const *const buffer, uint16_t size);

void uart_dma_driver_receive_start(uart_dma_driver_t *self, uart_dma_driver_rx_user_cb_t user_cb);

void uart_dma_driver_init(uart_dma_driver_t *self, uart_dma_driver_config_t *config);

/********************** End of CPP guard *************************************/
#ifdef __cplusplus
}
#endif

#endif /* INC_UART_DMA_DRIVER_H_ */
/********************** end of file ******************************************/

