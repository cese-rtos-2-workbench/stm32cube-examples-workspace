/*
 * Copyright (c) YEAR NOMBRE <MAIL>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : gpio_driver.c
 * @date   : May 24, 2022
 * @author : NOMBRE <MAIL>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include <arch.h>
#include <gpio_driver.h>

/********************** macros and definitions *******************************/

#define GPIO_PORT_                      (gpio_arch_descriptors_[id].port)
#define GPIO_PIN_                       (gpio_arch_descriptors_[id].pin)

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

typedef struct
{
    GPIO_TypeDef* port;
    uint16_t pin;
} gpio_arch_descriptor_t;

static gpio_arch_descriptor_t const gpio_arch_descriptors_[] =
{
  {LD1_GPIO_Port, LD1_Pin},
  {LD2_GPIO_Port, LD2_Pin},
  {LD3_GPIO_Port, LD3_Pin},
};

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

bool gpio_driver_read(gpio_dirver_id_t id)
{
  return (GPIO_PIN_SET == HAL_GPIO_ReadPin(GPIO_PORT_, GPIO_PIN_)) ? true : false;
}

void gpio_driver_write(gpio_dirver_id_t id, bool const state)
{
  HAL_GPIO_WritePin(GPIO_PORT_, GPIO_PIN_, (state) ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

void gpio_driver_toggle(gpio_dirver_id_t id)
{
  HAL_GPIO_TogglePin(GPIO_PORT_, GPIO_PIN_);
}

void gpio_driver_init(void)
{
  gpio_driver_write(GPIO_DRIVER_ID_LED1, false);
  gpio_driver_write(GPIO_DRIVER_ID_LED2, false);
  gpio_driver_write(GPIO_DRIVER_ID_LED3, false);
  return;
}

/********************** end of file ******************************************/
