/*
 * Copyright (c) 2022 SEBASTIAN BEDIN <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : routine_example_polling.c
 * @date   : May 18, 2022
 * @author : SEBASTIAN BEDIN <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include <app_arch.h>
#include <app_config.h>

/********************** macros and definitions *******************************/

#define PERIOD_ticks_                   (100 / APP_CONFIG_PERIOD_ms)
#define DELAY_ticks_                    (500 / APP_CONFIG_PERIOD_ms)

#define BUFFER_SIZE_                    (64)

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

static uint8_t buffer_[BUFFER_SIZE_];

static uint32_t buffer_len_;
static uint8_t buffer_user_[BUFFER_SIZE_];

/********************** external data definition *****************************/

extern UART_HandleTypeDef huart3;

/********************** internal functions definition ************************/

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
  if(&huart3 == huart)
  {
//    // RX
//    {
//      HAL_StatusTypeDef rx_status;
//      rx_status = HAL_UARTEx_ReceiveToIdle_IT(&huart3, buffer_, BUFFER_SIZE_);
//      if(HAL_OK != rx_status)
//      {
//        // error
//      }
//    }
  }
}

void buffer_process_(uint8_t const *const buffer, uint16_t size)
{
  buffer_len_ += size;
}

void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t size)
{
  static uint16_t old_size = 0;
  uint32_t len;

  if(&huart3 == huart)
  {
    if (size != old_size)
    {
      if (size > old_size)
      {
        len = size - old_size;
        buffer_process_(buffer_ + old_size, len);
      }
      else
      {
        len = BUFFER_SIZE_ - old_size;
        buffer_process_(buffer_ + old_size, len);

        if (0 < size)
        {
          len = size;
          buffer_process_(buffer_ , len);
        }
      }
    }
    old_size = size;
  }
}

static void init_(void *parameters)
{
  // clean UART
  HAL_UART_Receive(&huart3, buffer_, BUFFER_SIZE_, 100);

  buffer_len_ = 0;

  // RX
  {
    HAL_StatusTypeDef rx_status;
    rx_status = HAL_UARTEx_ReceiveToIdle_DMA(&huart3, buffer_, BUFFER_SIZE_);
    if(HAL_OK != rx_status)
    {
      // error
    }
  }
}

static void loop_(void *parameters)
{
  if(0 < buffer_len_)
  {
    // TX
    {
      __disable_irq();
      uint32_t len = buffer_len_;
      buffer_len_ = 0;
      __enable_irq();

      sprintf(buffer_user_, "Rx: %d Bytes\n", len);
      HAL_StatusTypeDef tx_status;
      tx_status = HAL_UART_Transmit_IT(&huart3, buffer_user_, strlen(buffer_user_) + 1);
      if(HAL_OK != tx_status)
      {
        // error
      }
    }
  }

  return;
}

/********************** external functions definition ************************/

void routine_example_dma(void *parameters)
{
    static bool _init = true;
    static uint32_t ticks = DELAY_ticks_;
    if(_init)
    {
      _init = false;
      init_(parameters);
    }

    if(0 == ticks)
    {
      ticks = (0 < PERIOD_ticks_) ? PERIOD_ticks_ : 1;
      loop_(parameters);
    }
    ticks--;
}

/********************** end of file ******************************************/
