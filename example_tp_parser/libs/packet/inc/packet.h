/*
 * common.h
 *
 *  Created on: May 27, 2022
 *      Author: seb
 */

#ifndef INC_PACKET_H_
#define INC_PACKET_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "string.h"
#include "packet_define.h"
#include "packet_protocol.h"
#include "packet_message.h"
#include "packet_datachecked.h"

//#define PACKET_MAX_SIZE                         (200)
//#define PACKET_MIN_SIZE                         (1 + 4 + 2 + 1) // SOM + ID + A + CRC + EOM
//#define PACKET_SOM                              ('(')
//#define PACKET_EOM                              (')')
//#define PACKET_CRC_SEED                         (0x00)

//struct packet_s
//{
//    packet_user_cb_t user_cb;
//    packet_feed_handler_t feed_h;
//
//    packet_byte_t *buffer;
//    size_t size;
//};

//void packet_init(packet_t *self, packet_user_cb_t user_cb, packet_byte_t *buffer);
//
//void packet_feed(packet_t *self, packet_byte_t byte);
//
//void packet_get_message(packet_t *self, packet_message_t *message);

#ifdef __cplusplus
}
#endif

#endif /* INC_PACKET_H_ */
