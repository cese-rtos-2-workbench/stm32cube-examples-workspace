/*
 * Copyright (c) YEAR NOMBRE <MAIL>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : statemachine.c
 * @date   : May 26, 2022
 * @author : NOMBRE <MAIL>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include <statemachine.h>

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

void statemachine_init(statemachine_t *self, statemachine_state_t *states, size_t states_size)
{
  self->current_state_id = 0;
  self->next_state_id = 0;
  self->last_state_id = states_size;
  self->states = states;
  self->states_size = states_size;
}

void statemachine_state_init(statemachine_t *self, int state_id, statemachine_event_cb_t entry_cb, statemachine_event_cb_t loop_cb, statemachine_event_cb_t exit_cb)
{
  self->states[state_id].entry_cb = entry_cb;
  self->states[state_id].loop_cb = loop_cb;
  self->states[state_id].exit_cb = exit_cb;
}

void statemachine_go_to_state(statemachine_t *self, int state_id)
{
  self->next_state_id = state_id;
}

void statemachine_loop(statemachine_t *self)
{
  statemachine_state_t *state = &(self->states[self->current_state_id]);

  if (self->current_state_id != self->last_state_id)
  {
//    state->event_cb(self, STATEMACHINE_EVENT_ENTRY);
    if(NULL != state->entry_cb)
    {
      state->entry_cb(self);
    }
    self->last_state_id = self->current_state_id;
  }

//  state->event_cb(self, STATEMACHINE_EVENT_LOOP);
  if(NULL != state->loop_cb)
  {
    state->loop_cb(self);
  }

  if (self->current_state_id != self->next_state_id)
  {
//    state->event_cb(self, STATEMACHINE_EVENT_EXIT);
    if(NULL != state->exit_cb)
    {
      state->exit_cb(self);
    }
    self->current_state_id = self->next_state_id;
  }
}

/********************** end of file ******************************************/
