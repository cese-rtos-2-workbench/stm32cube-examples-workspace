/*
 * Copyright (c) YEAR NOMBRE <MAIL>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : gpio_driver.c
 * @date   : May 24, 2022
 * @author : NOMBRE <MAIL>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include <arch.h>
#include <gpio_driver.h>

/********************** macros and definitions *******************************/

#define SCU_PORT_                       (gpio_arch_descriptors_[id].scu.port)
#define SCU_PIN_                        (gpio_arch_descriptors_[id].scu.pin)
#define SCU_FUN_                        (gpio_arch_descriptors_[id].scu.func)
#define GPIO_PORT_                      (gpio_arch_descriptors_[id].gpio.port)
#define GPIO_PIN_                       (gpio_arch_descriptors_[id].gpio.pin)

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

typedef struct
{
  struct
  {
    uint8_t port;
    uint8_t pin;
    int8_t func;
  } scu;
  struct
  {
    uint8_t port;
    uint8_t pin;
  } gpio;
} gpio_arch_descriptor_t;

static gpio_arch_descriptor_t const gpio_arch_descriptors_[] =
{
 {{2, 10, FUNC0}, {0, 14}},          // 40   LED1    LED1
 {{2, 11, FUNC0}, {1, 11}},          // 41   LED2    LED2
 {{2, 12, FUNC0}, {1, 12}},          // 42   LED3    LED3
};

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static void gpio_init_(gpio_dirver_id_t id)
{
  Chip_SCU_PinMux(SCU_PORT_, SCU_PIN_, SCU_MODE_INACT | SCU_MODE_ZIF_DIS | SCU_MODE_INBUFF_EN, SCU_FUN_);
  Chip_GPIO_SetPortDIROutput(LPC_GPIO_PORT, GPIO_PORT_, (1 << GPIO_PIN_));

  gpio_driver_write(id, false);
}

/********************** external functions definition ************************/

bool gpio_driver_read(gpio_dirver_id_t id)
{
  return Chip_GPIO_GetPinState(LPC_GPIO_PORT, GPIO_PORT_, GPIO_PIN_);
}

void gpio_driver_write(gpio_dirver_id_t id, bool const state)
{
  Chip_GPIO_SetPinState(LPC_GPIO_PORT, GPIO_PORT_, GPIO_PIN_, (state) ? 1 : 0);
}

void gpio_driver_toggle(gpio_dirver_id_t id)
{
  gpio_driver_write(id, !gpio_driver_read(id));
}

void gpio_driver_init(void)
{
  Chip_GPIO_Init(LPC_GPIO_PORT);
  gpio_init_(GPIO_DRIVER_ID_LED1);
  gpio_init_(GPIO_DRIVER_ID_LED2);
  gpio_init_(GPIO_DRIVER_ID_LED3);
  return;
}

/********************** end of file ******************************************/
